# Walker Smith
1580 Tappahannok Trail
Atlanta, GA 30062
706.577.6256
Walker@WalkerRandolphSmith.com
[http://Walker.codes](http://Walker.codes) | [LinkedIn](http://www.linkedin.com/in/walkerrandolphsmith)

## Summary
Technology enthusiast, proud Eagle Scout, and software craftsman, I have a passion for the web and software development. I strive to adhere to Agile and SOLID principles while always maintaining a constant pursuit of improvement. My most recent project is an Android application with accompanying Angular website to help prevent texting and driving. My interests include UI/UX engineering, Agile, and test driven development.

## Technical Overview

### Technologies
* JavaScript
* HTML5, CSS3/LESS
* Java
* C#
* SQL

### Skills
* MVC and MVVM patterns
* Android development
* CI and build automation fundamentals
* Communicates technical concepts effectively

### Frameworks and Libraries
* Angular
* browserify, requirejs
* npm, bower
* Knockout
* underscore
* Twitter Bootstrap
* gulp, grunt

### Tools
* Visual Studio and ReSharper
* WebStorm
* Git
* Android Studio
* PowerShell, command line
* Bamboo, AWS

## Projects

### [Yolo](http://yolosafely.com)
Android application aimed to reduce texting and driving among teenagers by notifying parents when their children are in a moving vehicle and with the option of remotely locking the child's device. An Angular web application accompanies the app to allow parents to monitor their children's devices from any device with a web browser.

### [JayLarkin](http://jaylarkin.com)
Web Application leveraging Knockout to promote car dealership sales. Inventory managed with Flickr API.

### [Hush](https://dl.dropboxusercontent.com/u/103371057/hush.apk)
Android automation application used to toggle the ringer mode of the device based on the current location. Leveraging various Google APIs including maps, places, autocomplete and integrating GPS, the app features searching for places by type, via voice, and address using autocompletion.

### [Faster](https://chrome.google.com/webstore/detail/faster/mpcocdniceegcnhmbkcicikapllibcob?authuser=1)
Chrome app used to train developers to type code faster and more accurately. Leveraging web technologies including Grunt, Less, Jade, and CoffeeScript.

### Risk
Pixel Sense (Surface 1.0) application to emulate the classic board game Risk. It leveraged the XNA framework and integrated touch gestures with Microsoft tags for interactive di.

## Experience
**Bluetube Inc.**
1123 Zonolite Rd NE \#22, Atlanta, GA 30306
Web Developer
Sept. 2014 - present



## Education
**Bachelors of Science, Computer Science**
*Columbus State University - 2014*  

* ACM officer *(2012-2013)*
* ACM programming challenges contestant *(2012-2014)*
